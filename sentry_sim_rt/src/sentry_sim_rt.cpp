/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_sim_rt/sentry_sim_rt.h"

namespace sentry_sim
{
SentrySimRt::SentrySimRt(int argc, char* argv[], const std::string& name)
  : DsProcess(argc, argv, name), thruster_(NUM_AUV_THRUSTERS), servo_(NUM_AUV_SERVOS)
{
  buoyancy_.buoyancy = 0.0;
}

void SentrySimRt::onThrusterCmdMsg(const ds_actuator_msgs::ThrusterCmd::ConstPtr& msg, int index)
{
  thruster_[index] = *msg;
}

void SentrySimRt::onServoCmdMsg(const ds_actuator_msgs::ServoCmd::ConstPtr& msg, int index)
{
  servo_[index] = *msg;
}

void SentrySimRt::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr& msg)
{
  aggregatedState_ = *msg;
  ds_nav_msgs::ModelState out = model_.runVehicleModel(aggregatedState_, thruster_, servo_, buoyancy_, 0.1);
  out.header.stamp = msg->header.stamp;
  pub_["ModelState"].publish(out);
}

void SentrySimRt::onBuoyancyMsg(const ds_nav_msgs::Buoyancy::ConstPtr& msg)
{
  buoyancy_ = *msg;
}

void SentrySimRt::setupParameters()
{
  ds_base::DsProcess::setupParameters();
}

void SentrySimRt::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();

  auto nh = nodeHandle();
  std::string aggregated_state_ns = ros::param::param<std::string>("~aggregated_state_ns", "0");
  sub_["Aggregated_state"] = nh.subscribe<ds_nav_msgs::AggregatedState>(
      aggregated_state_ns, 1, boost::bind(&SentrySimRt::onAggregatedStateMsg, this, _1));

  std::string buoyancy_ns = ros::param::param<std::string>("~buoyancy_ns", "0");
  sub_["Buoyancy"] =
      nh.subscribe<ds_nav_msgs::Buoyancy>(buoyancy_ns, 1, boost::bind(&SentrySimRt::onBuoyancyMsg, this, _1));

  std::string thruster_aft_port_ns = ros::param::param<std::string>("~thruster_aft_port_ns", "0");
  sub_["Thruster_aft_port"] = nh.subscribe<ds_actuator_msgs::ThrusterCmd>(
      thruster_aft_port_ns, 1, boost::bind(&SentrySimRt::onThrusterCmdMsg, this, _1, AFT_PORT));
  std::string thruster_aft_stbd_ns = ros::param::param<std::string>("~thruster_aft_stbd_ns", "0");
  sub_["Thruster_aft_stbd"] = nh.subscribe<ds_actuator_msgs::ThrusterCmd>(
      thruster_aft_stbd_ns, 1, boost::bind(&SentrySimRt::onThrusterCmdMsg, this, _1, AFT_STBD));
  std::string thruster_fwd_port_ns = ros::param::param<std::string>("~thruster_fwd_port_ns", "0");
  sub_["Thruster_fwd_port"] = nh.subscribe<ds_actuator_msgs::ThrusterCmd>(
      thruster_fwd_port_ns, 1, boost::bind(&SentrySimRt::onThrusterCmdMsg, this, _1, FWD_PORT));
  std::string thruster_fwd_stbd_ns = ros::param::param<std::string>("~thruster_fwd_stbd_ns", "0");
  sub_["Thruster_fwd_stbd"] = nh.subscribe<ds_actuator_msgs::ThrusterCmd>(
      thruster_fwd_stbd_ns, 1, boost::bind(&SentrySimRt::onThrusterCmdMsg, this, _1, FWD_STBD));

  std::string servo_fwd_ns = ros::param::param<std::string>("~servo_fwd_ns", "0");
  sub_["Servo_fwd"] = nh.subscribe<ds_actuator_msgs::ServoCmd>(
      servo_fwd_ns, 1, boost::bind(&SentrySimRt::onServoCmdMsg, this, _1, SERVO_FWD));
  std::string servo_aft_ns = ros::param::param<std::string>("~servo_aft_ns", "0");
  sub_["Servo_aft"] = nh.subscribe<ds_actuator_msgs::ServoCmd>(
      servo_aft_ns, 1, boost::bind(&SentrySimRt::onServoCmdMsg, this, _1, SERVO_AFT));
}

void SentrySimRt::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();

  std::string modelState_ns = ros::param::param<std::string>("~model_state_ns", "/model");
  auto nh = nodeHandle();
  pub_["ModelState"] = nh.advertise<ds_nav_msgs::ModelState>(modelState_ns, 1);
}

void SentrySimRt::setupTimers()
{
  ds_base::DsProcess::setupTimers();
}
}

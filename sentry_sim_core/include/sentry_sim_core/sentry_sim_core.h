/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_SIM_CORE_H
#define SENTRY_SIM_CORE_H

#include "ros/ros.h"
#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_actuator_msgs/ServoCmd.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_nav_msgs/Buoyancy.h"
#include "ds_nav_msgs/ModelState.h"

#define NUM_AUV_SERVOS 2
#define NUM_AUV_THRUSTERS 4

namespace sentry_sim
{
class State
{
public:
  State(ds_nav_msgs::AggregatedState agg, std::vector<ds_actuator_msgs::ThrusterCmd> thruster,
        std::vector<ds_actuator_msgs::ServoCmd> servo, ds_nav_msgs::Buoyancy buoyancy)
  {
    // Both agg and this state use N-E-D, fwd-stbd-down
    for (int i = 0; i < 6; i++)
    {
      etadot[i] = 0.0;
    }
    for (int i = 0; i < 3; i++)
    {
      nu1dot[i] = 0.0;
      nu2dot[i] = 0.0;
      tau1[i] = 0.0;
      tau2[i] = 0.0;
    }
    // Linear velocities
    nu1[0] = agg.surge_u.value;
    nu1[1] = agg.sway_v.value;
    nu1[2] = agg.heave_w.value;
    // Position
    eta1[0] = agg.northing.value;
    eta1[1] = agg.easting.value;
    eta1[2] = agg.down.value;
    // Heading
    eta2[2] = agg.heading.value;
    // Heading rate
    nu2[2] = agg.r.value;
    // weight
    weight = -buoyancy.buoyancy;
    // Servo commands
    for (int i = 0; i < NUM_AUV_SERVOS; i++)
      phi[i] = servo[i].cmd_radians;
    // Thruster commands
    for (int i = 0; i < NUM_AUV_THRUSTERS; i++)
      h[i] = thruster[i].cmd_newtons;
  };

  virtual ~State() = default;

  // standard notation as in Fossen
  // these are the inputs
  double phi[NUM_AUV_SERVOS];   // control surface angles
  double h[NUM_AUV_THRUSTERS];  // thruster inputs
  // the next force/torque computed in the sim
  double tau1[3], tau2[3];

  // these are the actual state updated by the sim
  double nu1dot[3], nu2dot[3];
  double nu1[3], nu2[3];
  double eta1[3], eta2[3];
  double etadot[6];
  // some extra items we might want to look at externally
  double body_alpha;  // angle of attack in uw plane
  double control_surface_alpha[NUM_AUV_SERVOS];
  double control_surface_lift[NUM_AUV_SERVOS];
  double control_surface_drag[NUM_AUV_SERVOS];
  double weight;

};

// TODO: verify that in the model this indexing is correct
typedef enum { SERVO_AFT = 1, SERVO_FWD = 0 } servo_id;

typedef enum { AFT_PORT = 0, AFT_STBD = 1, FWD_PORT = 2, FWD_STBD = 3 } thruster_id;

// Internal axis id of the simulation
typedef enum { U_AXIS = 0, V_AXIS = 1, W_AXIS = 2, P_AXIS = 3, Q_AXIS = 4, R_AXIS = 5 } axis_id;

class SentrySimCore
{
public:
  /// @brief Default constructor: should load dynamic parameters from parameter server
  SentrySimCore();
  virtual ~SentrySimCore() = default;

  /// @brief runs the vehicle model for a timestep dt
  ds_nav_msgs::ModelState runVehicleModel(ds_nav_msgs::AggregatedState agg,
                                          std::vector<ds_actuator_msgs::ThrusterCmd> thruster,
                                          std::vector<ds_actuator_msgs::ServoCmd> servo, ds_nav_msgs::Buoyancy buoyancy,
                                          double dt);

private:
  /// @brief returns the lift of a control surface (fin)
  double control_surface_lift(double alpha, double u);

  /// @brief returns the drag of a control surface (fin)
  double control_surface_drag(double alpha, double u);

  /// atan2 which returns 0 if both x and y are 0
  double atan2_check(double x, double y);

  // Surge parameters
  double Mu, Cdu;

  // Heave parameters
  double Mw, Cdw;

  // Rotation parameters
  double Izz, Cdr;

  // Control surface Lift
  double CS_Cl0, CS_Cl1, CS_Cl2;
  double CS_alpha1, CS_alpha2;

  // Control surface Drag model
  double CS_Cd0, CS_Cd1, CS_Cd2;

  // global parameters
  double CS_Area, CS_Rho;

  double Thruster_Moment_Arm[NUM_AUV_THRUSTERS];

  bool use_surge_limiter;
  double max_surge_value;
  double surge_limit_value;
  bool surge_limiter_active;
};
}

#endif

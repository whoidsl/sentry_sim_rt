/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_sim_core/sentry_sim_core.h"

#include <gtest/gtest.h>

class SentrySimCoreTest : public ::testing::Test
{
public:
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(SentrySimCoreTest, forwardMotion)
{
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[0].cmd_newtons = 100;
  thr[1].cmd_newtons = 100;
  thr[2].cmd_newtons = 100;
  thr[3].cmd_newtons = 100;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0;
  ser[1].cmd_radians = 0;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_GT(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_EQ(modelState.down, 0.0);
  ASSERT_GT(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_EQ(modelState.heave_w, 0.0);
}

TEST_F(SentrySimCoreTest, forwardUpMotion)
{
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[0].cmd_newtons = 100;
  thr[1].cmd_newtons = 100;
  thr[2].cmd_newtons = 100;
  thr[3].cmd_newtons = 100;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0.2;
  ser[1].cmd_radians = 0.2;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_GT(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_LT(modelState.down, 0.0);
  ASSERT_GT(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_LT(modelState.heave_w, 0.0);
}

TEST_F(SentrySimCoreTest, headingMotion)
{
  // Positive heading moment
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[sentry_sim::AFT_PORT].cmd_newtons = 100;
  thr[sentry_sim::AFT_STBD].cmd_newtons = -100;
  thr[sentry_sim::FWD_PORT].cmd_newtons = 0;
  thr[sentry_sim::FWD_STBD].cmd_newtons = 0;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0.0;
  ser[1].cmd_radians = 0.0;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_EQ(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_EQ(modelState.down, 0.0);
  ASSERT_EQ(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_EQ(modelState.heave_w, 0.0);
  ASSERT_GT(modelState.heading, 0.0);
}

TEST_F(SentrySimCoreTest, forwardMotionIteration)
{
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[0].cmd_newtons = 100;
  thr[1].cmd_newtons = 100;
  thr[2].cmd_newtons = 100;
  thr[3].cmd_newtons = 100;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0;
  ser[1].cmd_radians = 0;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_GT(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_EQ(modelState.down, 0.0);
  ASSERT_GT(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_EQ(modelState.heave_w, 0.0);

  // Transfer model state over for next iteration
  state.header = modelState.header;
  state.ds_header = modelState.ds_header;

  state.heading.valid = true;
  state.heading.value = modelState.heading;

  state.down.valid = true;
  state.down.value = modelState.down;

  state.easting.valid = true;
  state.easting.value = modelState.easting;

  state.northing.valid = true;
  state.northing.value = modelState.northing;

  state.pitch.valid = true;
  state.pitch.value = modelState.pitch;

  state.roll.valid = true;
  state.roll.value = modelState.roll;

  state.surge_u.valid = true;
  state.surge_u.value = modelState.surge_u;

  state.heave_w.valid = true;
  state.heave_w.value = modelState.heave_w;

  state.sway_v.valid = true;
  state.sway_v.value = modelState.sway_v;

  state.r.valid = true;
  state.r.value = modelState.r;

  state.p.valid = true;
  state.p.value = modelState.p;

  state.q.valid = true;
  state.q.value = modelState.q;

  auto modelState2 = model.runVehicleModel(state, thr, ser, buoy, 0.1);

  // The new heading value should not match the previous version.
  ASSERT_NE(modelState.northing, modelState2.northing);

}

TEST_F(SentrySimCoreTest, headingMotionIteration)
{

  // Positive heading moment
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[sentry_sim::AFT_PORT].cmd_newtons = 100;
  thr[sentry_sim::AFT_STBD].cmd_newtons = -100;
  thr[sentry_sim::FWD_PORT].cmd_newtons = 0;
  thr[sentry_sim::FWD_STBD].cmd_newtons = 0;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0.0;
  ser[1].cmd_radians = 0.0;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;

  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_EQ(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_EQ(modelState.down, 0.0);
  ASSERT_EQ(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_EQ(modelState.heave_w, 0.0);
  ASSERT_GT(modelState.heading, 0.0);

  // Transfer model state over for next iteration
  state.header = modelState.header;
  state.ds_header = modelState.ds_header;

  state.heading.valid = true;
  state.heading.value = modelState.heading;

  state.down.valid = true;
  state.down.value = modelState.down;

  state.easting.valid = true;
  state.easting.value = modelState.easting;

  state.northing.valid = true;
  state.northing.value = modelState.northing;

  state.pitch.valid = true;
  state.pitch.value = modelState.pitch;

  state.roll.valid = true;
  state.roll.value = modelState.roll;

  state.surge_u.valid = true;
  state.surge_u.value = modelState.surge_u;

  state.heave_w.valid = true;
  state.heave_w.value = modelState.heave_w;

  state.sway_v.valid = true;
  state.sway_v.value = modelState.sway_v;

  state.r.valid = true;
  state.r.value = modelState.r;

  state.p.valid = true;
  state.p.value = modelState.p;

  state.q.valid = true;
  state.q.value = modelState.q;

  auto modelState2 = model.runVehicleModel(state, thr, ser, buoy, 0.1);

  // The new heading value should not match the previous version.
  ASSERT_NE(modelState.heading, modelState2.heading);
}

TEST_F(SentrySimCoreTest, forwardUpCoastingMotion)
{
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[0].cmd_newtons = 0;
  thr[1].cmd_newtons = 0;
  thr[2].cmd_newtons = 0;
  thr[3].cmd_newtons = 0;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0.2;
  ser[1].cmd_radians = 0.2;
  ds_nav_msgs::Buoyancy buoy;
  ds_nav_msgs::AggregatedState state;
  state.surge_u.value = 1.0;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_GT(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_LT(modelState.down, 0.0);
  ASSERT_LT(modelState.surge_u, state.surge_u.value);
  ASSERT_GT(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_LT(modelState.heave_w, 0.0);
}

TEST_F(SentrySimCoreTest, buoyancyMotion)
{
  std::vector<ds_actuator_msgs::ThrusterCmd> thr(4);
  thr[0].cmd_newtons = 0;
  thr[1].cmd_newtons = 0;
  thr[2].cmd_newtons = 0;
  thr[3].cmd_newtons = 0;
  std::vector<ds_actuator_msgs::ServoCmd> ser(2);
  ser[0].cmd_radians = 0.0;
  ser[1].cmd_radians = 0.0;
  ds_nav_msgs::Buoyancy buoy;
  buoy.buoyancy = 100;
  ds_nav_msgs::AggregatedState state;
  ds_nav_msgs::ModelState modelState;
  
  sentry_sim::SentrySimCore model;

  modelState = model.runVehicleModel(state, thr, ser, buoy, 0.1);
  ASSERT_EQ(modelState.northing, 0.0);
  ASSERT_EQ(modelState.easting, 0.0);
  ASSERT_LT(modelState.down, 0.0);
  ASSERT_EQ(modelState.surge_u, 0.0);
  ASSERT_EQ(modelState.sway_v, 0.0);
  ASSERT_LT(modelState.heave_w, 0.0);
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "test");
  return RUN_ALL_TESTS();
}

/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_SIM_RT
#define SENTRY_SIM_RT

#include "ds_base/ds_process.h"
#include "sentry_sim_core/sentry_sim_core.h"

namespace sentry_sim
{
class SentrySimRt : public ds_base::DsProcess
{
public:
  SentrySimRt(int argc, char* argv[], const std::string& name);
  virtual ~SentrySimRt() = default;

  void onThrusterCmdMsg(const ds_actuator_msgs::ThrusterCmd::ConstPtr& msg, int index);

  void onServoCmdMsg(const ds_actuator_msgs::ServoCmd::ConstPtr& msg, int index);

  void onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr& msg);

  void onBuoyancyMsg(const ds_nav_msgs::Buoyancy::ConstPtr& msg);

protected:
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupTimers() override;

private:
  SentrySimCore model_;

  // Publishers, timers, subscribers
  std::unordered_map<std::string, ros::Publisher> pub_;
  std::unordered_map<std::string, ros::Subscriber> sub_;
  std::unordered_map<std::string, ros::Timer> tmr_;

  std::vector<ds_actuator_msgs::ThrusterCmd> thruster_;
  std::vector<ds_actuator_msgs::ServoCmd> servo_;
  ds_nav_msgs::Buoyancy buoyancy_;
  ds_nav_msgs::AggregatedState aggregatedState_;
};
}

#endif

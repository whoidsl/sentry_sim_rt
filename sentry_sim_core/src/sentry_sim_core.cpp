/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_sim_core/sentry_sim_core.h"

namespace sentry_sim
{
SentrySimCore::SentrySimCore()
{
  // Get parameters from the parameter server with default values relevant for Sentry
  ros::param::param<double>("~Mu", Mu, 2500.0);
  ros::param::param<double>("~Cdu", Cdu, 100.0);
  ros::param::param<double>("~Mw", Mw, 2500.0);
  ros::param::param<double>("~Cdw", Cdw, 50.0);
  ros::param::param<double>("~Izz", Izz, 1400.0);
  ros::param::param<double>("~Cdr", Cdr, 110.0);
  ros::param::param<double>("~CS_Cl0", CS_Cl0, 0.95);
  ros::param::param<double>("~CS_Cl1", CS_Cl1, 2.10);
  ros::param::param<double>("~CS_Cl2", CS_Cl2, -2.48);
  ros::param::param<double>("~CS_alpha1", CS_alpha1, 0.262);
  ros::param::param<double>("~CS_alpha2", CS_alpha2, 2.880);
  ros::param::param<double>("~CS_Cd0", CS_Cd0, 1.8);
  ros::param::param<double>("~CS_Cd1", CS_Cd1, 0.53);
  ros::param::param<double>("~CS_Cd2", CS_Cd2, 0.25);
  ros::param::param<double>("~CS_Area", CS_Area, 0.2);
  ros::param::param<double>("~CS_Rho", CS_Rho, 1000.0);
  ros::param::param<double>("~Thruster_Moment_Arm_Aft_Port", Thruster_Moment_Arm[AFT_PORT], 0.76);
  ros::param::param<double>("~Thruster_Moment_Arm_Aft_Stbd", Thruster_Moment_Arm[AFT_STBD], -0.76);
  ros::param::param<double>("~Thruster_Moment_Arm_Fwd_Port", Thruster_Moment_Arm[FWD_PORT], 0.76);
  ros::param::param<double>("~Thruster_Moment_Arm_Fwd_Stbd", Thruster_Moment_Arm[FWD_STBD], -0.76);
  ros::param::param<bool>("~use_surge_limiter", use_surge_limiter, false);
  ros::param::param<double>("~max_surge_value", max_surge_value, 0.9);
  ros::param::param<double>("~surge_limit_value", surge_limit_value, .65);
}

ds_nav_msgs::ModelState SentrySimCore::runVehicleModel(ds_nav_msgs::AggregatedState agg,
                                                       std::vector<ds_actuator_msgs::ThrusterCmd> thruster,
                                                       std::vector<ds_actuator_msgs::ServoCmd> servo,
                                                       ds_nav_msgs::Buoyancy buoyancy, double dt)
{
  // This model uses a N-E-D fwd-stbd-down frame. This frame is the same as the one in AggregatedState output by the
  // ds_nav_aggregator package

  // Conventions used in the actuator_msgs input to this function:
  // - thruster command is positive if it generates a positive forward thrust
  // - servo command is 0 when fins are zeroed, positive fins up, negative fins down

  State s(agg, thruster, servo, buoyancy);

  double ch, sh;
  // compute control surface forces and drags
  s.body_alpha = atan2_check(s.nu1[2], s.nu1[0]);  // positive is down
  // a bit wierd of a sign thing
  // phi is positive up, body_alpha positive down,
  // control_surface_alpha positive down (so positive lift makes
  // positive fw
  double u = s.nu1[0];
  double w = s.nu1[2];
  double vel = sqrt(u * u + w * w);
  for (int i = 0; i < NUM_AUV_SERVOS; i++)
  {
    s.control_surface_alpha[i] = s.body_alpha + s.phi[i];
    u = s.nu1[0];
    s.control_surface_lift[i] = control_surface_lift(s.control_surface_alpha[i], vel);
    s.control_surface_drag[i] = control_surface_drag(s.control_surface_alpha[i], vel);
  }

  // u axis
  for (int i = 0; i < NUM_AUV_THRUSTERS; i++)
  {
    int k;
    // DY 04/11/2007
    // thrusters 0 and 1 are tilted by servo 0, thrusters 2 and 3 titled by servo 1
    k = i / 2;
    s.tau1[U_AXIS] += s.h[i] * cos(s.phi[k]);
  }
  for (int i = 0; i < NUM_AUV_SERVOS; i++)
  {
    s.tau1[U_AXIS] -= (s.control_surface_drag[i] * cos(s.body_alpha) - s.control_surface_lift[i] * sin(s.body_alpha));
  }
  s.nu1dot[0] = (1.0 / Mu) * (s.tau1[0] - Cdu * s.nu1[0] * fabs(s.nu1[0]));  // 2008-07-06 mvj Added fabs().

  // w axis
  s.tau1[W_AXIS] += s.weight;
  // positive control surface tilt is up, so flip the sign so
  // negative tilt makes positive fw (down)
  for (int i = 0; i < NUM_AUV_THRUSTERS; i++)
  {
    int k;
    k = i / 2;  // 2008-07-06    mvj    Fixed same bug as was fixed by earlier above.
    s.tau1[W_AXIS] -= s.h[i] * sin(s.phi[k]);
  }
  for (int i = 0; i < NUM_AUV_SERVOS; i++)
  {
    s.tau1[W_AXIS] -= (s.control_surface_drag[i] * sin(s.body_alpha) + s.control_surface_lift[i] * cos(s.body_alpha));
  }
  s.nu1dot[2] = (1.0 / Mw) * (s.tau1[2] - Cdw * s.nu1[2] * fabs(s.nu1[2]));  // 2008-07-06 mvj Added fabs().

  // r axis
  s.tau2[2] += s.h[0] * cos(s.phi[0]) * Thruster_Moment_Arm[0];
  s.tau2[2] += s.h[1] * cos(s.phi[0]) * Thruster_Moment_Arm[1];
  s.tau2[2] += s.h[2] * cos(s.phi[1]) * Thruster_Moment_Arm[2];
  s.tau2[2] += s.h[3] * cos(s.phi[1]) * Thruster_Moment_Arm[3];
  s.nu2dot[2] = (1.0 / Izz) * (s.tau2[2] - Cdr * s.nu2[2] * fabs(s.nu2[2]));  // 2008-07-06 mvj Added fabs().

  // integrate
  for (int i = 0; i < 3; i++)
  {
    s.nu1[i] += s.nu1dot[i] * dt;
    s.nu2[i] += s.nu2dot[i] * dt;
  }
  // cheap trashy condom to prevent blowup when fins are vertical
  if (s.nu1[2] > 3.0)
  {
    s.nu1[2] = 3.0;
  }
  if (s.nu1[2] < -3.0)
  {
    s.nu1[2] = -3.0;
  }

  // xform
  ch = cos(s.eta2[2]);
  sh = sin(s.eta2[2]);
  s.etadot[U_AXIS] = s.nu1[0] * ch - s.nu1[1] * sh;
  s.etadot[V_AXIS] = s.nu1[0] * sh + s.nu1[1] * ch;
  s.etadot[W_AXIS] = s.nu1[2];
  s.etadot[R_AXIS] = s.nu2[2];

  // integrate
  for (int i = 0; i < 3; i++)
  {
    s.eta1[i] += s.etadot[i] * dt;
    s.eta2[i] += s.etadot[i + 3] * dt;
  }

  ds_nav_msgs::ModelState out;
  out.northing = s.eta1[0];
  out.easting = s.eta1[1];
  out.down = s.eta1[2];
  out.heading = s.eta2[2];
  out.pitch = s.eta2[1];
  out.roll = s.eta2[0];
  //ROS_ERROR_STREAM_THROTTLE(30, "SURGE LIMITER : "<<use_surge_limiter<<" w/ max surge: "<<max_surge_value);
  if (use_surge_limiter && s.nu1[U_AXIS] >= max_surge_value) {
	  out.surge_u = surge_limit_value;
	  surge_limiter_active = true;
	  ROS_ERROR_STREAM("Limiting surge_u value ("<<s.nu1[U_AXIS]<<") to max_surge_value: "<<surge_limit_value);
  }
  else if (!use_surge_limiter && s.nu1[U_AXIS] >= max_surge_value) {
	  out.surge_u = s.nu1[U_AXIS];
	  surge_limiter_active = false;
          ROS_WARN_STREAM_THROTTLE(30,"Surge_u value ("<<s.nu1[U_AXIS]<<") > max_surge_value: "<<max_surge_value<<"but surge limiter inactive, using calculated val "<<out.surge_u);
  }
  else {
	  out.surge_u = s.nu1[U_AXIS];
	  surge_limiter_active = false;
	  ROS_WARN_STREAM_THROTTLE(30, "Using calculated surge_u value: "<<out.surge_u);
  }
  out.sway_v = s.nu1[V_AXIS];
  out.heave_w = s.nu1[W_AXIS];
  out.q = s.nu2[Q_AXIS];
  out.p = s.nu2[P_AXIS];
  out.r = s.nu2[R_AXIS];
  out.du_dt = s.nu1dot[U_AXIS];
  out.dv_dt = s.nu1dot[V_AXIS];
  out.dw_dt = s.nu1dot[W_AXIS];
  out.dq_dt = s.nu2dot[Q_AXIS];
  out.dp_dt = s.nu2dot[P_AXIS];
  out.dr_dt = s.nu2dot[R_AXIS];
  out.surge_limited = surge_limiter_active;
  return out;
}

double SentrySimCore::control_surface_lift(double alpha, double u)
{
  double Cl, absalpha, result;
  absalpha = fabs(alpha);
  Cl = CS_Cl0 * sin(2.0 * absalpha);
  if (absalpha <= CS_alpha1)
    Cl += CS_Cl1 * absalpha;
  if (absalpha >= CS_alpha2)
    Cl += CS_Cl2 * (M_PI - absalpha);
  result = 0.5 * CS_Rho * CS_Area * Cl * u * u;
  if (alpha < 0.0)
    result = -result;
  return result;
}

double SentrySimCore::control_surface_drag(double alpha, double u)
{
  double Cd, absalpha, da;
  absalpha = fabs(alpha);
  da = M_PI / 2.0 - absalpha;
  Cd = CS_Cd0 * exp(-(da * da) / (2.0 * CS_Cd1 * CS_Cd1)) + CS_Cd2 * (1.0 - cos(4.0 * absalpha));
  return (0.5 * CS_Rho * CS_Area * Cd * u * u);
}

double SentrySimCore::atan2_check(double x, double y)
{
  if ((x == 0.0) && (y == 0.0))
    return 0.0;
  else
    return atan2(x, y);
}
}
